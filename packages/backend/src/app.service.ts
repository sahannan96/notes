import { Injectable, NotFoundException } from "@nestjs/common";
import { DataModel } from "./app.model";

@Injectable()
export class AppService {
  private dataModel: DataModel[] = [];

  create(title: string, desc: string) {
    const id = this.uniqueId();
    const newData = new DataModel(id, title, desc);
    this.dataModel.push(newData);
    //console.log(this.dataModel);
    return newData;
  }

  showAll() {
    return [...this.dataModel];
  }

  showOne(id: string) {
    // const foundIndex = this.findElement(id);
    // if(foundIndex >= 0) {
    //   return this.dataModel[foundIndex];
    // } else {
    //   return new NotFoundException("Element not found");
    // }
  }

  edit(id: string, titleNew: string, descNew: string) {
    const foundIndex = this.findElementIndex(id);

    if (foundIndex >= 0) {
      this.dataModel[foundIndex].title = titleNew;
      this.dataModel[foundIndex].description = descNew;
      // if (titleNew) {
      //   this.dataModel[foundIndex].title = titleNew;
      // }
      // if (descNew) {
      //   this.dataModel[foundIndex].description = descNew;
      // }
      return this.dataModel[foundIndex];
    } else {
      return new NotFoundException("Element not found");
    }
  }

  delete(id: string) {
    const elementIndex = this.findElementIndex(id);
    //console.log("delete id number " + elementIndex);

    if (elementIndex >= 0) {
      //const index = this.dataModel[elementIndex];
      this.dataModel.splice(elementIndex, 1);
      return "Element removed";
    } else {
      return new NotFoundException("Element not found");
    }
  }

  findElementIndex(id: string) {
    //console.log("ID in find " + id);
    const ElementIndex = this.dataModel.findIndex((x) => x.id === id);
    //console.log("findElementIndex " + ElementIndex);
    return ElementIndex;
  }

  uniqueId() {
    const id = (Math.floor(Math.random() * 100) + 1).toString();
    if (this.dataModel[0] != null) {
      for (var counter = 0; counter < this.dataModel.length; counter++) {
        if (this.dataModel[counter].id === id) {
          console.log(id + " 1");
          this.uniqueId();
        } else {
          return id;
        }
      }
    } else {
      return id;
    }
  }
}
