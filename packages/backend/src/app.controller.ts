import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from "@nestjs/common";
import { AppService } from "./app.service";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  create(@Body("title") title: string, @Body("description") desc: string) {
    const createdElement = this.appService.create(title, desc);
    return createdElement;
  }

  @Get()
  showAll() {
    return this.appService.showAll();
  }

  @Get(":id")
  showOne(@Param("id") id: string) {
    const singleElement = this.appService.showOne(id);
    return singleElement;
  }

  @Patch(":id")
  edit(
    @Param("id") id: string,
    @Body("title") titleNew: string,
    @Body("description") descNew: string
  ) {
    console.log(id + "  " + titleNew + "  " + descNew);
    const editedElement = this.appService.edit(id, titleNew, descNew);
    return editedElement;
  }

  @Delete(":id")
  delete(@Param("id") id: string) {
    this.appService.delete(id);
  }
}
