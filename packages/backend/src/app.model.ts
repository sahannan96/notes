export class DataModel {
  constructor(
    public id: string,
    public title: string,
    public description: string
  ) {}
}
