"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataModel = void 0;
class DataModel {
    constructor(id, title, description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }
}
exports.DataModel = DataModel;
//# sourceMappingURL=app.model.js.map