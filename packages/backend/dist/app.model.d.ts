export declare class DataModel {
    id: string;
    title: string;
    description: string;
    constructor(id: string, title: string, description: string);
}
