"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const common_1 = require("@nestjs/common");
const app_model_1 = require("./app.model");
let AppService = class AppService {
    constructor() {
        this.dataModel = [];
    }
    create(title, desc) {
        const id = this.uniqueId();
        const newData = new app_model_1.DataModel(id, title, desc);
        this.dataModel.push(newData);
        return newData;
    }
    showAll() {
        return [...this.dataModel];
    }
    showOne(id) {
    }
    edit(id, titleNew, descNew) {
        const foundIndex = this.findElementIndex(id);
        if (foundIndex >= 0) {
            this.dataModel[foundIndex].title = titleNew;
            this.dataModel[foundIndex].description = descNew;
            return this.dataModel[foundIndex];
        }
        else {
            return new common_1.NotFoundException("Element not found");
        }
    }
    delete(id) {
        const elementIndex = this.findElementIndex(id);
        if (elementIndex >= 0) {
            this.dataModel.splice(elementIndex, 1);
            return ("Element removed");
        }
        else {
            return new common_1.NotFoundException("Element not found");
        }
    }
    findElementIndex(id) {
        const ElementIndex = this.dataModel.findIndex(x => x.id === id);
        return ElementIndex;
    }
    uniqueId() {
        const id = (Math.floor(Math.random() * 100) + 1).toString();
        if (this.dataModel[0] != null) {
            for (var counter = 0; counter < this.dataModel.length; counter++) {
                if (this.dataModel[counter].id === id) {
                    console.log(id + " 1");
                    this.uniqueId();
                }
                else {
                    return id;
                }
            }
        }
        else {
            return id;
        }
    }
};
AppService = __decorate([
    common_1.Injectable()
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map