import { AppService } from './app.service';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    create(title: string, desc: string): import("./app.model").DataModel;
    showAll(): import("./app.model").DataModel[];
    showOne(id: string): void;
    edit(id: string, titleNew: string, descNew: string): import("./app.model").DataModel | import("@nestjs/common").NotFoundException;
    delete(id: string): void;
}
