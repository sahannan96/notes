import { NotFoundException } from '@nestjs/common';
import { DataModel } from './app.model';
export declare class AppService {
    private dataModel;
    create(title: string, desc: string): DataModel;
    showAll(): DataModel[];
    showOne(id: string): void;
    edit(id: string, titleNew: string, descNew: string): DataModel | NotFoundException;
    delete(id: string): NotFoundException | "Element removed";
    findElementIndex(id: string): number;
    uniqueId(): string;
}
