import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Note } from './note';
import { AppService } from './app.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app.dialog',
  templateUrl: 'app.dialog.html',
  styleUrls: ['./app.component.css'],
})
export class Dialog {
  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: Note,
    public snackBar: MatSnackBar,
    private appService: AppService
  ) {}

  colors = ['red', 'yellow', 'blue'];

  editNote(data: any) {
    if (data.title.length == 0 && data.description.length == 0) {
      this.deleteNote(data);
    } else {
      this.appService.update(data).subscribe(() => {});
      this.snackBar.open('Note ', 'Edited', { duration: 1000 });
    }
  }

  deleteNote(data: any) {
    this.appService.delete(data).subscribe(() => {});
    this.snackBar.open('Note ', 'Deleted', { duration: 1000 });
  }
}
