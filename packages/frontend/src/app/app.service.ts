import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Note } from './note';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  public Note = {};

  constructor(private httpClient: HttpClient) {}

  create(title: string, description: string) {
    return this.httpClient.post<any>('http://localhost:3000/', {
      title,
      description,
    });
  }

  readAll() {
    return this.httpClient.get<Note[]>('http://localhost:3000/');
  }

  update(note: any) {
    const body = { title: note.title, description: note.description };
    console.log(
      'id: ' +
        note.id +
        '<br>' +
        'title: ' +
        note.title +
        '<br>' +
        'desc: ' +
        note.description
    );
    return this.httpClient.patch<Note[]>(
      'http://localhost:3000/' + note.id,
      body
    );
  }

  delete(note: any) {
    return this.httpClient.delete<Note[]>('http://localhost:3000/' + note.id);
  }
}
