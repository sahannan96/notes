import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppService} from './app.service';
import { MatDialog } from '@angular/material/dialog';
import { Dialog } from './app.dialog';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(
    private appService: AppService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  public title = "";
  public desc= "";



  noteForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl('')
  });

  notes: any;
  //displayedColumns: string[] = ['id', 'title', 'desc', 'actions'];

  createNote() {
    if ( this.noteForm.controls.title.value.length == 0  && this.noteForm.controls.description.value.length == 0 ) {
      this.snackBar.open('Cannot create a blank note', '', { duration: 1000 });
    } else {
      console.log( this.noteForm.controls.title.value, 
        this.noteForm.controls.description.value )
      this.appService.create(
        this.noteForm.controls.title.value, 
        this.noteForm.controls.description.value
        )
        .subscribe(() => this.readNotes());
        this.snackBar.open('Note ', 'Created', { duration: 1000 });
    }
  }

  readNotes() {
    this.appService.readAll()
    .subscribe(
      data => 
      this.notes = data
      );
      console.log(this.notes.id);
  }

  // editNote() {}

  // deleteNote(note: any) {
  //   this.appService.delete(note)
  //   .subscribe(() => this.readNotes());
  //   this.snackBar.open('Note ', 'De', { duration: 1000 });
  // }

  openDialog(note: any) {
    this.title = note.title;
    this.desc = note.description;
    const dialogRef = this.dialog.open(Dialog, {data: note});

    dialogRef.afterClosed()
    .subscribe(() => this.readNotes());
  }






  ngOnInit() {
    this.readNotes();
  }
}

